import React from 'react';

import Layout from './containers/Layout/Layout';
import Home from './containers/Home/Home';

function App() {
  return (
    <div>
      <Layout>
        <Home></Home>
      </Layout>
    </div>
  );
}

export default App;
