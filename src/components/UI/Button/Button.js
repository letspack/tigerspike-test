import React from 'react';
import PropTypes from 'prop-types';
import classes from './Button.module.scss';

const button = ( props ) => (
  <button className={[classes.Button, classes[props.buttonType]].join(' ')} onClick={props.clicked}>{props.children}</button>
);

button.propTypes = {
  clicked: PropTypes.func,
  buttonType: PropTypes.string.isRequired,
};

export default button;
