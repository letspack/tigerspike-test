import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classes from './Modal.module.scss';
import Aux from '../../../hoc/Aux/Aux';
import Backdrop from '../Backdrop/Backdrop';

class Modal extends Component {

  shouldComponentUpdate( nextProps, nextState, nextContext ) {
    console.log( '[Modal] shouldComponentUpdate nextProps=', nextProps );
    return nextProps.show !== this.props.show || nextProps.children !== this.props.children;
  }

  componentWillUpdate( nextProps, nextState, nextContext ) {
    console.log( '[Modal] componentWillUpdate' );
  }

  render() {
    return (
      <Aux>
        <Backdrop clicked={ this.props.onBackDropClicked } show={ this.props.show }/>
        <div
          className={ classes.Modal }
          style={ {
            transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',
            opacity: this.props.show ? 1 : 0,
          } }>
          <span onClick={ this.props.onBackDropClicked } className={ classes.CloseModal }>X</span>
          { this.props.children }
        </div>
      </Aux>
    );
  }
}

Modal.propTypes = {
  show: PropTypes.bool,
  onBackDropClicked: PropTypes.func,
};

export default Modal;
