import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Modal from './Modal';
import Backdrop from '../Backdrop/Backdrop'

configure( { adapter: new Adapter() } );


describe( '<Modal />', () => {
  let wrapper = null;
  const props = {
    show: false,
    onBackDropClicked: () => {
    }
  };

  beforeEach( () => {


    wrapper = shallow( <Modal { ...props } /> );
  } );

  it( 'should render Modal element', () => {
    wrapper.setProps( {
      link: 'myLink',
      active: true,
      children: 'Home'
    } );

    expect( wrapper.find( Backdrop ).prop( 'clicked' ) ).toEqual( props.onBackDropClicked );
    expect( wrapper.find( Backdrop ).prop( 'show' ) ).toEqual( props.show );
    expect( wrapper.find( 'div.Modal' ).prop( 'style' ) ).toEqual( {
      opacity: 0,
      transform: 'translateY(-100vh)'
    } );
  } );
} );
