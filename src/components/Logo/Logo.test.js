import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Logo from './Logo';

configure( { adapter: new Adapter() } );


describe( '<Logo />', () => {
  let wrapper = null;
  beforeEach( () => {
    wrapper = shallow( <Logo link=''/> );
  } );

  it( 'should contain an img element', () => {
    expect( wrapper.find( 'img' ).prop('src') ).toEqual('tigerspike-logo.png' );
  } );
} );
