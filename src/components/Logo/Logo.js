import React from 'react';
import PropTypes from 'prop-types';

import burgerLogo from '../../assets/images/tigerspike-logo.png';
import classes from './Logo.module.scss';

const logo = ( props ) => (
  <div className={classes.Logo} style={{ height: props.height }}>
    <a href="/">
      <img src={burgerLogo} alt="tigerSpike-test"/>
    </a>
  </div>
);

logo.propTypes = {
  height: PropTypes.string
};

export default logo;
