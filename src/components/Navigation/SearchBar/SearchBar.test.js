import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import SearchBar from './SearchBar';

configure( { adapter: new Adapter() } );


describe( '<SearchBar />', () => {
  let wrapper = null;
  let instance = null;
  beforeEach( () => {

    wrapper = shallow( <SearchBar /> );
    instance = wrapper.instance();
  } );

  it( 'should render SearchBar element', () => {
    expect( wrapper.find( 'input' ).prop('placeholder') ).toEqual('Search Image...' );
  } );
} );
