import React, { useContext } from 'react';
import SearchBarContext from '../../../context/searchBar-context';
import classes from './SearchBar.module.scss';

const SearchBar = () => {

  const searchBarContext = useContext( SearchBarContext );

  return (
    <div className={ classes.SearchBar }>
      <input type="text"
             placeholder="Search Image..."
             onChange={ ( event ) => searchBarContext.updateSearch( event.target.value ) }/>
    </div>
  );
};

SearchBar.propTypes = {
};

export default SearchBar;
