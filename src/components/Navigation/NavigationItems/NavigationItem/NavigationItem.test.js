import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import NavigationItem from './NavigationItem';

configure( { adapter: new Adapter() } );


describe( '<NavigationItem />', () => {
  let wrapper = null;
  beforeEach( () => {
    wrapper = shallow( <NavigationItem link=''/> );
  } );

  it( 'should render NavigationItem element', () => {
    wrapper.setProps( {
      link: 'myLink',
      active: true,
      children: 'Home'
    } );

    expect( wrapper.find( 'a' ).prop('href') ).toEqual('myLink' );
    expect( wrapper.find( 'a' ).prop('className') ).toEqual('active' );
    expect( wrapper.find( 'a' ).text() ).toEqual('Home' );
  } );
} );
