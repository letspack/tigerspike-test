import React from 'react';
import classes from './Toolbar.module.scss';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import SearchBar from '../SearchBar/SearchBar';


const toolbar = ( props ) => (
  <header className={ classes.Toolbar }>
    <div className={ classes.LeftToolbar }>
      <div className={ classes.Logo }>
        <Logo/>
      </div>
      <nav>
        <NavigationItems/>
      </nav>
    </div>
    <SearchBar/>
  </header>
);

toolbar.propTypes = {};

export default toolbar;
