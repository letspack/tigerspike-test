import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classes from './ImagePreview.module.scss';
import Spinner from '../UI/Spinner/Spinner';
import Aux from '../../hoc/Aux/Aux';

class ImagePreview extends Component {

  state = {
    showSpinner: true
  };

  handleImageLoaded = () => {
    this.setState( {
      showSpinner: false
    } );
  };

  handleImageErrored = () => {
    this.setState( {
      showSpinner: false
    } );
  };

  render() {

    let spinner = <Spinner />;
    let imageDetails = null;

    if ( !this.state.showSpinner ) {

      spinner = null;

      imageDetails = (
        <Aux>
          <p><strong>Location:</strong> { this.props.image.location }</p>
          <p><strong>Date:</strong> { this.props.image.createdAt }</p>
        </Aux>
      )
    }

    return (
      <div className={ classes.ImagePreview }>
        <h2 style={ { textAlign: 'center' } }>{ this.props.image.title }</h2>
        { spinner }
        <img
          onLoad={ this.handleImageLoaded }
          onError={ this.handleImageErrored }
          src={ this.props.image.urlBig }
          alt={ this.props.image.title }/>
        { imageDetails }
      </div>
    );
  }

}


ImagePreview.propTypes = {
  image: PropTypes.object
};

export default ImagePreview;
