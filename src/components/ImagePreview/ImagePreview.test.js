import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import ImagePreview from './ImagePreview';

configure( { adapter: new Adapter() } );


describe( '<ImagePreview />', () => {
  let wrapper = null;
  let instance = null;
  beforeEach( () => {

    const image = {
      location: 'USA',
      title: 'Yellowstone',
      createdAt: '2019-05-09',
      urlBig: 'https://urlBig.png',
    };

    wrapper = shallow( <ImagePreview image={image}/> );
    instance = wrapper.instance();
  } );

  it( 'should render ImagePreview element', () => {
    jest.spyOn( instance, 'handleImageLoaded' );
    instance.forceUpdate();
    expect( wrapper.find( 'img' ).prop('src') ).toEqual('https://urlBig.png' );

    wrapper.find( 'img' ).simulate( 'load' );

    expect( instance.handleImageLoaded ).toHaveBeenCalled();
    expect( wrapper.contains( <strong>Location:</strong> ) ).toEqual(true );
  } );
} );
