import React from 'react';
import PropTypes from 'prop-types';
import Button from '../UI/Button/Button';
import classes from './Welcome.module.scss';

const welcome = ( props ) => {
  return (
    <div className={ classes.Welcome }>
      <h1>Welcome !!!</h1>
      <p>On this website, you will find some of the best pictures I took during my travels.</p>
      <p>Feel free to filter them using the search bar on the top right,</p>
      <p>You can click on any of them to see more details.</p>
      <Button clicked={ props.startNavigation } buttonType={ 'Success' }> START </Button>
    </div>
  );
};

welcome.propTypes = {
  startNavigation: PropTypes.func
};

export default welcome;
