import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classes from './ImageThumbnail.module.scss';
import Spinner from '../../UI/Spinner/Spinner';

class ImageThumbnail extends Component {

  state = {
    showSpinner: true
  };

  handleImageLoaded = () => {
    this.setState( {
      showSpinner: false
    } );
  };

  handleImageErrored = () => {
    this.setState( {
      showSpinner: false
    } );
  };

  render() {

    let spinner = <Spinner />;

    if ( !this.state.showSpinner ) {
      spinner = null;
    }

    return (
      <div
        className={ classes.ImageThumbnail }
        onClick={ this.props.clicked }  >
        { spinner }
        <img
          onLoad={ this.handleImageLoaded }
          onError={ this.handleImageErrored }
          src={ this.props.image.urlSmall }
          alt={ this.props.image.title } />
        <p>{ this.props.image.title }</p>
      </div>
    );
  }
}

ImageThumbnail.propTypes = {
  image: PropTypes.object,
  clicked: PropTypes.func,
};

export default ImageThumbnail;
