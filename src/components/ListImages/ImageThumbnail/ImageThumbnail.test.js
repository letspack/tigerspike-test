import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import ImageThumbnail from './ImageThumbnail';

configure( { adapter: new Adapter() } );


describe( '<ImageThumbnail />', () => {
  let wrapper = null;
  let instance = null;
  beforeEach( () => {

    const image = {
      location: 'USA',
      title: 'Yellowstone',
      createdAt: '2019-05-09',
      urlBig: 'https://urlBig.png',
      urlSmall: 'https://urlSmall.png',
    };

    wrapper = shallow( <ImageThumbnail image={image}/> );
    instance = wrapper.instance();
  } );

  it( 'should render ImageTumbnail element', () => {
    jest.spyOn( instance, 'handleImageLoaded' );
    instance.forceUpdate();
    expect( wrapper.find( 'img' ).prop('src') ).toEqual('https://urlSmall.png' );

    wrapper.find( 'img' ).simulate( 'load' );

    expect( instance.handleImageLoaded ).toHaveBeenCalled();
    expect( wrapper.find( 'p' ).text() ).toEqual('Yellowstone' );
  } );
} );
