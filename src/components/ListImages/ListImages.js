import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import classes from './ListImages.module.scss';

import ImageThumbnail from './ImageThumbnail/ImageThumbnail';
import SearchBarContext from '../../context/searchBar-context';

const ListImages = ( props ) => {

  const searchBarContext = useContext( SearchBarContext );

  const images = props.images
    .filter( ( image ) => {
      // console.log( '[ListImages] image=', image );
      return image.title.toLowerCase().indexOf( searchBarContext.searchText.toLowerCase() ) !== -1;
    } )
    .map( ( image ) => {
    return <ImageThumbnail clicked={ () => props.imageSelected( image ) } key={ image.id } image={ image }/>
  } );

  return (
    <div className={ classes.ListImages }>
      { images }
    </div>
  );
};

ListImages.propTypes = {
  images: PropTypes.array,
  imageSelected: PropTypes.func,
};

export default ListImages;
