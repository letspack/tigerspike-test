import React from 'react';

import { configure, mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import ListImages from './ListImages';
import ImageThumbnail from './ImageThumbnail/ImageThumbnail';

configure( { adapter: new Adapter() } );

describe( '<ListImages />', () => {
  let wrapper;
  let instance;

  const images = [
    {
      id: 1,
      location: 'USA',
      title: 'Yellowstone',
      createdAt: '2019-05-09',
      urlBig: 'https://urlBig.png',
      urlSmall: 'https://urlSmall.png',
    },
    {
      id: 3,
      urlBig: 'https://urlBig.png',
      urlSmall: 'https://urlSmall.png',
      title: 'Machu Picchu',
      location: 'Peru',
      createdAt: '2014-02-13',
    },
  ];

  beforeEach( () => {
    wrapper = shallow( <ListImages images={ images }/> );
    instance = wrapper.instance();
  } );


  it( 'should render ListImages element', () => {

    expect( wrapper.find( ImageThumbnail ) ).toHaveLength(2);
  } );
} );
