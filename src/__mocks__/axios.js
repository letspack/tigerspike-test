'use strict';
module.exports = {


  create: jest.fn( function() {
    return this;
  } ),

  get: jest.fn( ( url ) => {
    if ( url === '/images.json' ) {
      return Promise.resolve( {
        data: [
          {
            id: 1,
            urlBig: 'https://UrlBig.png',
            urlSmall: 'https://UrlSmall.png',
            title: 'Bryce Canyon',
            location: 'USA',
            createdAt: '2014-07-25'
          },
          {
            id: 2,
            urlBig: 'https://UrlBig.png',
            urlSmall: 'https://UrlSmall.png',
            title: 'Yellowstone - Lower waterfall',
            location: 'USA',
            createdAt: '2014-07-10'
          }
        ]
      } );
    }
    else {
      return Promise.resolve( {
        data: []
      });
    }

  } )
};
