import React, { Component } from 'react';

import Modal from '../../components/UI/Modal/Modal';
import Aux from '../Aux/Aux';

const WithErrorHandler = ( WrappedComponent, axios ) => {
  return class extends Component {

    constructor( props ) {
      super(props);

      this.requestInterceptor = axios.interceptors.request.use( (req) => {
        this.setState( { error: null } );
        return req;
      } );

      this.responseInterceptor =  axios.interceptors.response.use( res => res, (error) => {
        this.setState( { error: error } );
      } );
    }

    componentWillUnmount() {
      axios.interceptors.request.eject( this.requestInterceptor );
      axios.interceptors.response.eject( this.responseInterceptor );
    }

    state = {
      error: null
    };

    hideModalHandler = () => {
      this.setState( { error: null } );
    };

    render() {
      return (
        <Aux>
          <Modal
            onBackDropClicked={ this.hideModalHandler }
            show={ !!this.state.error } >
            { this.state.error ? this.state.error.message : null }
          </Modal>
          <WrappedComponent { ...this.props }>

          </WrappedComponent>
        </Aux>
      )
    }

  }
};


export default WithErrorHandler;
