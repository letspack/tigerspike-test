import React, { Component } from 'react';
import Aux from '../../hoc/Aux/Aux';
import classes from './Layout.module.scss';

import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SearchBarContext from '../../context/searchBar-context';

class Layout extends Component {

  state = {
    searchText: ''
  };

  updateSearchTextHandler = ( text ) => {
    this.setState( { searchText: text } )
  };

  render() {
    return (
      <Aux>

        <SearchBarContext.Provider value={ {
          searchText: this.state.searchText,
          updateSearch: this.updateSearchTextHandler
        } }>
          <Toolbar/>
          <main className={ classes.Content }>
            { this.props.children }
          </main>
        </SearchBarContext.Provider>
      </Aux>
    );
  }
}

export default Layout;
