import React, { Component } from 'react';
import axios from '../../axios-tigerspike';

import Aux from '../../hoc/Aux/Aux';
import Modal from '../../components/UI/Modal/Modal';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import Spinner from '../../components/UI/Spinner/Spinner';

import ListImages from '../../components/ListImages/ListImages';
import ImagePreview from '../../components/ImagePreview/ImagePreview';
import Welcome from '../../components/Welcome/Welcome';

export class Home extends Component {

  state = {
    images: null,
    error: false,
    showModal: false,
    imageSelected: null,
    loading: true,
    welcomeMessage: true,
  };

  componentDidMount() {
    console.log( '[Home] componentDidMount' );
    axios.get( '/images.json' )
      .then( ( response ) => {
        // console.log( '[Home] componentDidMount - response=', response );
        this.setState( { images: response.data } )
      } )
      .catch( ( error ) => {
        console.log( '[Home] componentDidMount error=', error );
        this.setState( { error: true } );
      })
      .finally( () => {
        this.setState( { loading: false } )
      });
  }

  hideWelcomeMessage = () => {
    this.setState( {
      welcomeMessage: false,
    } );
  };

  hideModal = () => {
    this.setState( {
      showModal: false,
      imageSelected: null
    } );
  };

  selectImageHandler = ( image ) => {
    console.log( '[Home] selectImageHandler image=', image );
    this.setState( {
      showModal: true,
      imageSelected: image
    } );
  };

  render() {

    let modalContent = (
      <ImagePreview image={ this.state.imageSelected }/>
    );

    if ( !this.state.imageSelected ) {
      modalContent = null;
    }

    let listImages = this.state.error ? <p>Images can't be loaded</p> : <Spinner />;

    if ( this.state.images ) {
      listImages = <ListImages imageSelected={ this.selectImageHandler } images={ this.state.images }/>;
    }

    if ( this.state.welcomeMessage ) {
      listImages = <Welcome  startNavigation={ this.hideWelcomeMessage }/>
    }

    return (
      <Aux>
        <Modal
          onBackDropClicked={ this.hideModal }
          show={ this.state.showModal }>
          { modalContent }
        </Modal>
        { listImages }
      </Aux>
    )
  }
}

export default withErrorHandler( Home, axios );
