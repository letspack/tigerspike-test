import React from 'react';

import { configure, mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import axios from 'axios';

import { Home } from './Home';

configure( { adapter: new Adapter() } );

describe( '<Home />', () => {
  let wrapper;
  let instance;

  let getSpy;

  beforeEach( () => {

    getSpy = jest.spyOn(axios, 'get');

    wrapper = shallow( <Home /> );
    instance = wrapper.instance();
  } );


  it( 'should render Home Component', () => {
    expect( instance.state.images ).toEqual([
      {
        id: 1,
        urlBig: 'https://UrlBig.png',
        urlSmall: 'https://UrlSmall.png',
        title: 'Bryce Canyon',
        location: 'USA',
        createdAt: '2014-07-25'
      },
      {
        id: 2,
        urlBig: 'https://UrlBig.png',
        urlSmall: 'https://UrlSmall.png',
        title: 'Yellowstone - Lower waterfall',
        location: 'USA',
        createdAt: '2014-07-10'
      }
    ]);
    expect( getSpy ).toHaveBeenCalled();
  } );

  it( 'should change the state of showModal when selectImageHandler is called', () => {
    instance.selectImageHandler();
    expect( instance.state.showModal ).toEqual(true );
  } );

  it( 'should change the state of showModal when hideModal is called', () => {
    instance.setState( {
      showModal: true,
    } );
    instance.hideModal();
    expect( instance.state.showModal ).toEqual(false );
  } );
} );
