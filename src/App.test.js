import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import App from './App';
import Layout from './containers/Layout/Layout'

configure( { adapter: new Adapter() } );


describe( '<NavigationItems />', () => {

  it('renders without crashing', () => {
    const wrapper = shallow( <App /> );
    expect( wrapper.find( Layout ) ).toHaveLength(1);
  });
} );
