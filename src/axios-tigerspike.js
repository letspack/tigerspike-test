import axios from 'axios';

const instance = axios.create( {
  baseURL: 'https://tigerspike-test-abc24.firebaseio.com/'
  // baseURL: 'https://react-my-burger-81a09.firebaseio.com/'
});

export default instance;
