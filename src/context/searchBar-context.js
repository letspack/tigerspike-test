import React from 'react';

const authContext = React.createContext( {
  searchText: '',
  updateSearch: () => {},
} );

export default authContext;
